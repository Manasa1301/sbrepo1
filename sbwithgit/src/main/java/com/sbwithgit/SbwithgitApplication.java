package com.sbwithgit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbwithgitApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbwithgitApplication.class, args);
	}

}
